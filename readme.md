# Lab: Develop Code Repository and Push Commits 



### Description:

In this lab, we will learn how to develop a Git lab Repository, generate a repository for a series of  functions and push the changes to a GitLab Repository

You will split up into  groups and modify your own branches

The following outlines the structure of the lab: 

1. **Clone the repo and create a group branch**
	* Step 1. clone repo 
	* Step 2. create and checkout a branch
	* Step 3. make and commit a change to readme.txt
	* Step 4. push your changes back to GitLab
	* Step 5. merge your changes on GitLab.com
	* Step 6. check out your groups branch
	* step 7. working with issues 

## 1. 

For an overview on the topics we are reviewing in this lab, please review the following resources: 

Git:

* [the git book](https://git-scm.com/book/en/v2)
* [bitbucket explains git](https://www.atlassian.com/git)

Data camp courses:

* [Introduciton to git ](https://learn.datacamp.com/courses/introduction-to-git)
* [further comand line learning](https://learn.datacamp.com/courses?technologies=Git&technologies=Shell)

GitHub desktop: [Committing and Reviewing Changes to your Project](https://help.github.com/en/desktop/contributing-to-projects/committing-and-reviewing-changes-to-your-project)

### 1.1 clone repo 

The first step is to "Clone" the repository. This copy's the files to your local computer and creates a connection to repository. 

From the command you can type the following command: 

```bash
git clone <repo url> #replace <repo url> with the url of the reposiotry on gitlab
```



From GitHub desktop you can select clone from the file drop down your use the shortcut `Ctrl+shift+O`:

![clone](./img/gui-clone.png)





### 1.2 Create and checkout a branch 

<img src="./img/branch.png" width="500">

Once we have cloned the repository, we will create a new **branch** with our *name* and  move our git instance to this branch by **checking** it out. This will allow us to make and save changes without affecting the "master" branch.

From the command line, 

1. navigate into the repo. 

   ```bash
   cd <repo name> # cd stands for change directory and moves us to the folder we specify <repo name>
   ```

   

2. Create a new branch and check it out we use the checkout command.
```bash
git checkout -b <new-branch>  # the -b flag allows us to create a new repo and check it out in the
						    # same step. don't forget to replace <new-branch> with your name

git checkout <branch-name> 	 # once a branch is created we no longer need to us the -b flag to 								   # access it.    
```

From GitHub desktop, click the current branch dropdown and then select new branch  

![branch](./img/gui-branch.png)



### 1.3 make and commit a change to hellogroup.txt

We are now going to modify this repository and save those changes into git by **Committing ** them to the repository.  This will allow us to share our changes back to the repository on gitlab.com.	


We will now make an update to repository:
1. Open the hellogroup.txt file located in the base directory of the repository
2. Write a message for your group
3.  Save the file
4.  Commit those changes 

From the command line to commit changes we must first stage them with `git add` . once 
```bash
git add . # add all files that have changes 
git status # see the changes that are staged
git commit -m "put your commit message here" # commit your changes with a message

```



On GitHub desktop we use the commit button on the bottom left of the screen. 

![commit](./img/gui-commit.png)

### 1.4  Push your changes back to GitLab 

Once we have have committed our changes to the Local repository we want to share them so our group can see the nice message we wrote them. to do this we **push** the changes. 



From the command line, we use the `push` command. because this is the first time you are pushing this repo and because our new branch doesn't yet exist on github.com we have to use the `-u` option ,which sets the upstream branch so git knows where to push our updates in the future, and we need to specific the remote: origin (the default) and branch name. 


```bash
git push -u origin <branchname> # push your changes to a new remote branch
```



On GitHub desktop the publish branch button will push our changes to a new branch on Github.com  

![push](./img/gui-push.png)


### 1.5 Merge changes 

Once our branch is published to github.com we want to merge those changes back to the master branch. 



#### 1.5.1 Open a pull request on the repo  https://github.com/yeshivadataanalytics/lab3>

![pull request](./img/GitHub-pullrequest-newpr.png)

git

#### 1.5.2 Select your groups branch and the one that you just made


![select branch](./img/GitHub-pullrequest-branch.png)



#### 1.5.3 Resolve any conflicts


![conflicts](./img/GitHubpullrequestresolve.png)



#### 1.5.4 commit the merge and close the Pull Request


![commit](./img/GitHub-pullrequest-commit.png)

<br>



### 1.6  check out your groups branch before moving on to part 2

before we move to part 2 we will update our local repository and check out our groups branch
```bash 
git fetch -all #get updates to all branches
git checkout  {group_0} # replace with your groups name 

```
from GitHub desktop 

first run fetch:

![](img\gui-fetch.png)

then select your groups branch from the branch drop down:

![branch](./img/gui-branch.png)

<b>Congratulations! You are now a Git lab collaborator!</b>